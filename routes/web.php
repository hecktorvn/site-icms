<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

<<<<<<< HEAD
Route::namespace('Site')->group(function(){
    Route::get('/', 'HomeController')->name('site.home');

    Route::get('/products', 'CategoryController@index')->name('site.products');
    Route::get('/products/{slug}', 'CategoryController@show')->name('site.products.category');

    Route::get('/blog', 'BlogController')->name('site.blog');

    Route::view('/about', 'site.about.index')->name('site.about');

    Route::get('/contact', 'ContactController@index')->name('site.contact');
    Route::post('/contact', 'ContactController@form')->name('site.contact.form');
=======
Route::get('/', function () {
    return view('welcome');
>>>>>>> b2d5bb9ef9ee6fedd6a29a7f8d6db21d0b65b833
});
